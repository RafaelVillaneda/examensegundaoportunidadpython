'''
Created on 18 dic. 2020

@author: Rafael Villaneda
'''
nodes = ('A', 'B', 'C', 'D', 'E', 'F', 'G')
distances = {
       'B': {'A': 5, 'D': 1, 'G': 2},
       'A': {'B': 5, 'D': 3, 'E': 12, 'F' :5},
       'D': {'B': 1, 'G': 1, 'E': 1, 'A': 3},
       'G': {'B': 2, 'D': 1, 'C': 2},
       'C': {'G': 2, 'E': 1, 'F': 16},
       'E': {'A': 12, 'D': 1, 'C': 1, 'F': 2},
       'F': {'A': 5, 'E': 2, 'C': 16}}


#-------------------------------------- obtenido: 
# Python3 program for Bellman-Ford's  
# single source shortest path algorithm. 
from sys import maxsize 
  
# The main function that finds shortest 
# distances from src to all other vertices 
# using Bellman-Ford algorithm. The function 
# also detects negative weight cycle 
# The row graph[i] represents i-th edge with 
# three values u, v and w. 
def BellmanFord(graph, V, E, src): 
  
    # Initialize distance of all vertices as infinite. 
    dis = [maxsize] * V 
  
    # initialize distance of source as 0 
    dis[src] = 0
  
    # Relax all edges |V| - 1 times. A simple 
    # shortest path from src to any other 
    # vertex can have at-most |V| - 1 edges 
    for i in range(V - 1): 
        for j in range(E): 
            if dis[graph[j][0]] + graph[j][2] < dis[graph[j][1]]: 
                dis[graph[j][1]] = dis[graph[j][0]] + graph[j][2] 

    # check for negative-weight cycles. 
    # The above step guarantees shortest 
    # distances if graph doesn't contain 
    # negative weight cycle. If we get a 
    # shorter path, then there is a cycle. 
    for i in range(E): 
        x = graph[i][0] 
        y = graph[i][1] 
        weight = graph[i][2] 
        if dis[x] != maxsize and dis[x] + weight < dis[y]: 
            print("Graph contains negative weight cycle") 
  
    print("Vertex Distance from Source") 
    for i in range(V): 
        print("%d\t\t%d" % (i, dis[i])) 
  
#----------------------------------------
import sys
INF = sys.maxsize
class Warshall:
    def floydWarshall(self, graph):
        n = len(graph)
        dist = [[] for i in range(n)]
        for i in range(n):
            for j in range(n):
                dist[i].append(graph[i][j])
        for k in range(n):
            for i in range(n):
                for j in range(n):
                    dist[i][j] = min(dist[i][j],dist[i][k]+dist[k][j])
        print('Shortest Distance between every pair of vertex:-')
        for i in range(n):
            for j in range(n):
                if dist[i][j]==INF:
                    print ("%7s" % ("INF"),end=' ')
                else:
                    print ("%7s" % (dist[i][j]),end=' ')
            print()
#---------------------------------------



menu=False

while(menu==False):
    print("1- Algoritmo DIJKSTRA")
    print("2- Algoritmo FLOYD-WARSHAL")
    print("3- Algoritmo BELLMAN-FORD")
    print("4- Salir")
    op=input()
    if(op=="1"):
        #DIJKSTRA----------------OBTENIDO DE: 
        unvisited = {node: None for node in nodes} #using None as +inf
        visited = {}
        current = 'B'
        currentDistance = 0
        unvisited[current] = currentDistance
    
        while True:
            for neighbour, distance in distances[current].items():
                if neighbour not in unvisited: continue
                newDistance = currentDistance + distance
                if unvisited[neighbour] is None or unvisited[neighbour] > newDistance:
                    unvisited[neighbour] = newDistance
            visited[current] = currentDistance
            del unvisited[current]
            if not unvisited: break
            candidates = [node for node in unvisited.items() if node[1]]
            current, currentDistance = sorted(candidates, key = lambda x: x[1])[0]
        
        print(visited)
        print()
    elif(op=="2"):
        graph = [[0,5,INF,10],[INF,0,3,INF],[INF,INF,0,1],[INF,INF,INF,0]]
        warsh = Warshall()
        warsh.floydWarshall(graph)
    elif(op=="3"):
        
        V = 5 # Number of vertices in graph 
        E = 8 # Number of edges in graph 
      
        # Every edge has three values (u, v, w) where 
        # the edge is from vertex u to v. And weight 
        # of the edge is w. 
        graph = [[0, 1, -1], [0, 2, 4], [1, 2, 3],  
                 [1, 3, 2], [1, 4, 2], [3, 2, 5], 
                 [3, 1, 1], [4, 3, -3]] 
        BellmanFord(graph, V, E, 0) 
  
# This code is contributed by 
# sanjeev2552
    elif(op=="4"):
        print("Referencias:")
        print("DIJKSTRA: user3504080. (06/04/2014). Dijkstra's algorithm in python. 18/12/2020, de Stackoverrun Sitio web: https://stackoverrun.com/es/q/6268715")
        print("FLOYD-WARSHAL: https://cppsecrets.com/users/5629115104105118971091101011031055657495564103109971051084699111109/Python-Floyd-Warshall-Algorithm.php")
        print("BELLMAN-FORD: Anónimo. (15/05/2020). Algoritmo Bellman Ford (implementación simple). 18/12/2020, de geeksforgeeks Sitio web: https://www.geeksforgeeks.org/bellman-ford-algorithm-simple-implementation/")
        print("Saliendo.....")
        menu=True
    
